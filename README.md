# Calipsa challenge

## Prerequisites
- Docker v18
- Node.js v12

## RUN
- `docker-compose up -d` should build and run the containers.
