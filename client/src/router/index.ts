import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Lobby from '../components/Lobby.vue'
import Room from '../components/Room.vue'
import CreateRoom from '../components/CreateRoom.vue'
import Login from '../components/Login.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  { path: '/', component: Lobby },
  { path: '/room/:id', component: Room },
  { path: '/createRoom', component: CreateRoom },
  { path: '/login', component: Login }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
