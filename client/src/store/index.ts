import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: () => ({
    authenticationData: {
      hasUserCredentials: false,
      currentUser: {},
      user: '',
      password: ''
    },
    applicationState: {
      rooms: [],
      players: []
    },
    roomState: {
      gameStarted: false,
      wordChosen: true,
      currentQuestion: null,
      currentRoom: {
        createdBy: { name: '' }
      },
      currentGame: {
        started: false
      }
    }
  }),
  mutations: {
    setApplicationState (state, payload) {
      state.applicationState = payload
    },
    setAuthenticationData (state, payload) {
      state.authenticationData = payload
    },
    setRoomState (state, payload) {
      console.log('setting room state')
      console.log(payload)
      state.roomState = payload
    },
    setRoomInfo (state, roomInfo) {
      console.log({ roomInfo })
      state.roomState.currentRoom = roomInfo;
    }
  },
  getters: {
    getApplicationState (state) {
      return state.applicationState
    },
    getAuthenticationData (state) {
      return state.authenticationData
    },
    getRoomState (state) {
      return state.roomState
    }
  }
})
