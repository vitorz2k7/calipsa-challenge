import * as express from 'express';
import * as socketIo from 'socket.io';
import { createServer, Server } from 'http';
import { LobbyEvent } from './events/lobby.event';
import { ServerEvent } from './events/server.event';
import { Action } from './model/action';
import { Room } from './model/room';
import { Player } from './model/player';
import { ReturnModelType } from '@typegoose/typegoose';
import { ActionResponse } from './model/action-response';
var cors = require('cors');
var socketAuth = require('socketio-auth');
import 'reflect-metadata';
import { RoomEvent } from './events/room.events';
import { Game } from './model/game';
import { GameEvent } from './events/game.event';
import { Play } from './model/play';
import { closeRoom, createRoom, getRoomInfo, joinRoom, pushGameToRoom } from './functions/room.functions';
import { startNewGame, chooseGameWord, makePlay } from './functions/game.functions';
import { registerPlayer, resolvePlayer } from './functions/player.functions';
import { getApplicataionState } from './functions/lobby.functions';
export class GameServer {
  public static readonly PORT: number = 8080;
  private _app: express.Application;
  private server: Server;
  private io: SocketIO.Server;
  private port: string | number;
  private socketAuthenticator: any;

  constructor(
    private readonly roomModel: ReturnModelType<typeof Room, {}>,
    private readonly playerModel: ReturnModelType<typeof Player, {}>,
    
  ) {
    this._app = express();
    this.port = process.env.PORT || GameServer.PORT;
    this._app.use(cors());
    this._app.options('*', cors());
    this.server = createServer(this._app);
    this.initSocket();
  }
  private initSocket(): void {
    this.io = socketIo(this.server);
  }
  private async listen(): Promise<void> {
    
    const authenticate = async (socket: any, data: any, callback: any) => {

      const { user, password, name, register } = data;
      
      try {
        if (register) {
          const newPlayer = await registerPlayer(user, password, name, this.playerModel)
          console.log(`${newPlayer.user} has registered`)
          socket.emit('player_register_successful', newPlayer);
        } else {
          const authUser = await resolvePlayer(user, this.playerModel);

          if (authUser && authUser.validPassword(password)) {
            return callback(null, authUser);
          }
          callback({ message: 'Invalid credentials' })
        }
      } catch (error) {
        callback(error);
      }
    };

    const postAuthenticate = async (socket: any, data: any) => {

      const loggedUser = await resolvePlayer(data.user, this.playerModel);
      socket.client.user = loggedUser;
      socket.join('lobby');

      console.info(`Authenticated client connected client on port ${this.port} with socket-id: ${socket.conn.id} and logged as ${loggedUser.user}`);

      const timestamp = new Date().getTime();
      const currentSocketId = socket.conn.id;

      const buildResponse = (responseFields: {}, payload: Action): ActionResponse => Object.assign({
        timestamp,
        action: payload,
        status: 'response_starting',
        data: {}
      }, responseFields) as ActionResponse;

      const respondToAction = async (response: ActionResponse, socketId: string) => {

        (this.io.clients().connected[socketId])
          ? this.io.clients().connected[socketId].emit('action_response', response)
          : console.warn('could not deliver the response to the clientSocket')

        const appState = await getApplicataionState(this.io.clients().connected, this.roomModel, this.playerModel);
        this.io.emit('application_state', appState);
      }
      socket.on(LobbyEvent.PLAYER_CONNECTED, async (payload: any) => {
        const appState = await getApplicataionState(this.io.clients().connected, this.roomModel, this.playerModel);
        this.io.emit('application_state', appState);
      });
      // ACTIONS
      socket.on(ServerEvent.CLIENT_ACTION, async (payload: Action) => {
        console.info(`[${timestamp}] ${payload.client.user} triggered  the action: ${payload.event} ...`);

        if (payload.event === LobbyEvent.PLAYER_CREATE_ROOM) {
          const data = await createRoom(payload.data.room, loggedUser, this.roomModel, this.playerModel)
          const response = buildResponse({ data, status: 'action_success' }, payload);
          const roomId = data.room._id
          socket.join(roomId)
          await respondToAction(response, currentSocketId)
        }
        if (payload.event === LobbyEvent.PLAYER_JOIN_ROOM) {
          try { 
            const data = await joinRoom(payload.data.room, loggedUser, this.roomModel, this.playerModel);
            const response = buildResponse({ data, status: 'action_success' }, payload);
            const roomId = data.room._id
            socket.join(roomId)
            const currentGame = {
              started: false,
              finished: false,
              hasWord: false,
            }  
            this.io.to(roomId).emit(RoomEvent.ROOM_READY, { currentGame, currentRoom: data.room })
            this.io.to(roomId).emit('room_data', data.room)
            await respondToAction(response, currentSocketId)
          } catch (error) {
            const response = buildResponse({ data: error, status: 'error'}, payload);
            await respondToAction(response, currentSocketId)
          }
        };
      });

      socket.on(RoomEvent.CLOSE_ROOM, async (payload: any) => {
        const closedRoom = await closeRoom(payload.roomId, this.roomModel)
        this.io.to(closedRoom._id).emit(RoomEvent.ROOM_CLOSED, closedRoom)
      }),

      socket.on(RoomEvent.ROOM_INFO, async (payload: any) => {
        const roomInfo = await getRoomInfo(payload.roomId, this.roomModel)
        this.io.to(roomInfo._id).emit('room_data', roomInfo)
      }),
      socket.on(RoomEvent.ROOM_ACTION, async (payload: Action) => {
        const { currentGame, currentRoom } = payload.data.roomState;
        const socketRoom = this.io.to(currentRoom._id);
        
        if (payload.event === GameEvent.START_GAME) {
          const newGame = startNewGame(currentRoom);
          socketRoom.emit(GameEvent.GAME_STARTED, { currentRoom, currentGame: newGame });
        }

        if (payload.event === GameEvent.CHOOSE_WORD) {
          const newGameState = chooseGameWord(payload.data.chosenWord, currentGame);
          socketRoom.emit(GameEvent.WORD_CHOOSEN, { currentRoom, currentGame: newGameState });
          socketRoom.emit(GameEvent.QUESTION_TIME, { currentRoom, currentGame: newGameState });
        }

        if (payload.event === GameEvent.MAKE_PLAY) {
          const currentPlay: Play = payload.data.currentPlay;
          const afterMakePlay = makePlay(currentGame, currentPlay);

          (currentPlay.question && !currentPlay.wordGuess)
              ? socketRoom.emit(GameEvent.RESPONSE_TIME, { currentRoom, currentGame: afterMakePlay })
              : socketRoom.emit(GameEvent.QUESTION_TIME, { currentRoom, currentGame: afterMakePlay });

          if (afterMakePlay.finished) {
            const roomWithGame = pushGameToRoom(currentRoom._id, afterMakePlay, this.roomModel)
            socketRoom.emit(GameEvent.GAME_FINISH, { currentRoom: roomWithGame, currentGame: afterMakePlay });
          }
        };
      })
      const appState = await getApplicataionState(this.io.clients().connected, this.roomModel, this.playerModel);
      this.io.emit('application_state', appState);

    };
    this.socketAuthenticator = socketAuth(
      this.io,
      {
        authenticate,
        postAuthenticate,
        disconnect: () => {
          console.log('disconnected from the authenticator...');
        },
        timeout: 'none'
      });

    this.server.listen(this.port, () => {
      console.info('Running server on port %s', this.port);
    });

    this.io.on(ServerEvent.CONNECT, async (socket: any) => {
      socket.on(ServerEvent.DISCONNECT, () => {
        console.info('Client disconnected');
      });

      socket.on('error', (error: any) => {
        console.log(error)
      })
      console.info(`Unauthenticated client onnected client on port ${this.port} with socket-id: ${socket.conn.id}`);
    });
  };

  public async connect(): Promise<GameServer> {
    await this.listen();
    return this;
  };

  get app(): express.Application {
    return this._app;
  };
};
