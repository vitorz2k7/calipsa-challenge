export enum LobbyEvent {
  PLAYER_CONNECTED = 'player_connected',
  PLAYER_CREATE_ROOM = 'player_create_room',
  PLAYER_JOIN_ROOM = 'player_join_room',
  PLAYER_LEFT_ROOM = 'player_left_room',
  LIST_GAME_ROOMS = 'list_game_rooms',
  LIST_ALL_ROOMS = 'list_all_rooms',
  LIST_LOBBY_PLAYERS = 'list_loby_players',
  LIST_ALL_PLAYERS = 'list_all_players',
  LOBBY_UPDATE = 'lobby_update',
  NEW_ROOM_ADDED = 'new_room_added'
}
