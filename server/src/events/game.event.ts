
export enum GameEvent {
  CHOOSE_WORD = 'choose_word',
  WORD_CHOOSEN = 'word_choosen',
  START_GAME = 'start_game',
  GAME_STARTED = 'game_started',
  GAME_FINISH = 'game_finish',
  MAKE_PLAY = 'make_play',
  QUESTION_TIME = 'question_time',
  RESPONSE_TIME = 'response_time'
}