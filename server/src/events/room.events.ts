export enum RoomEvent {
  ROOM_READY = 'room_ready',
  ROOM_ACTION = 'room_action',
  ROOM_UPDATE = 'room_update',
  GAME_READY = 'game_ready',
  ROOM_INFO = 'room_info',
  CLOSE_ROOM = 'close_room',
  ROOM_CLOSED = 'room_closed'
}
