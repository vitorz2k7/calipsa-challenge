export enum ServerEvent {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect',
  CLIENT_ACTION = 'client_action',
  ACTION_RESPONSE = 'action_response'
}
