
export interface GameMessage {
  author: string;
  message: string;
}
