import { prop, Ref } from '@typegoose/typegoose';
var bcrypt = require('bcrypt');

export class Player {
  @prop({required: true})
  name: string;
  @prop({ unique: true })
  user: string;
  @prop({required: true})
  private hashedPassword: string;

  public validPassword(password: string) {
    return bcrypt.compareSync(password, this.hashedPassword);
  }

  public set password(password: string) {
    this.hashedPassword  = bcrypt.hashSync(password, 12);
  }
}