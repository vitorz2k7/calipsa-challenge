import { Question } from "./question";

export interface Play {
  question: Question;
  wordGuess: string | null;
}