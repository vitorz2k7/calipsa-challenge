import { Action } from "./action";

export class ActionResponse {
  action: Action;
  timestamp: number;
  status: string;
  data?: {} | [] | any;
} 