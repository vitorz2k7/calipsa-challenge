import { prop, modelOptions, Ref } from '@typegoose/typegoose';
import { Game } from './game';
import { Player } from './player';

@modelOptions({
  schemaOptions: {}
})
export class Room {
  @prop({required: true, default: 'room without name'})
  public name: string;
  @prop({ type: () => Player, ref: Player })
  public createdBy?: Ref<Player>;
  @prop({type: () => Player || null, default: null, ref: Player })
  public opponent?: Ref<Player> | null;
  @prop({type: () => [Game]})
  public games: Ref<Game>[];
  @prop({ default: true })
  public active!: boolean;
  @prop({ default: false })
  public full!: boolean;
}
