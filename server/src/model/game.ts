import { Player } from "./player";
import { Question } from "./question";

export class Game {
  started: boolean;
  finished: boolean;
  hasWord: boolean;
  word: string;
  currentQuestion: Question | null;
  questions: Question[] = [];
  winner: Player | null = null;
  wordThief: Player;
  detective: Player;
}
