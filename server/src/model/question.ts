export interface Question {
  question: string;
  response: boolean | null;
}
