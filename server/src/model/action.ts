import { Player } from "./player";

export class Action {
  client: Player;
  event: string;
  data?: {}| []| any;
}