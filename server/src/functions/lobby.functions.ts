import { ReturnModelType } from "@typegoose/typegoose";
import { Player } from "../model/player";
import { Room } from "../model/room";


const resolveWaitingRooms = async (
  roomModel: ReturnModelType<typeof Room, {}>
) => {
  const result = await roomModel
    .find({ active: true, full: false })
    .populate('createdBy')
    .limit(100)
    .exec();
  return result;
};

const getConnectedUserSocketIds = (sockets: any) => Object.keys(sockets)

const getApplicataionState = async (
  connectedSockets: any,
  roomModel: ReturnModelType<typeof Room, {}>,
  playerModel: ReturnModelType<typeof Player, {}>,
) => {
  const players = getConnectedUserSocketIds(connectedSockets).map((socketId: string) => {
    return connectedSockets[socketId].client.user;
  });
  const rooms = await resolveWaitingRooms(roomModel);

  const appState = { players, rooms };
  return appState
};

export { getApplicataionState }