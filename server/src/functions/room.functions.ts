import { ReturnModelType } from '@typegoose/typegoose';
import { Room } from '../model/room';
import { Player } from '../model/player';
import { Game } from '../model/game';

const createRoom = async(
  room: Room,
  player: Player,
  roomModel: ReturnModelType<typeof Room, {}>,
  playerModel: ReturnModelType<typeof Player, {}>
) => {

  const createdBy = await playerModel.findOne({ user: player.user });
  room.active = true;
  room.games = [];
  room.opponent = null;
  room.createdBy = createdBy;
  room.full = false;
  const createdRoom = await roomModel.create(room);
  createdRoom.save();
  console.info(`created room ${createdRoom.name} & ${createdRoom._id}`);
  console.info(`createdBy: ${createdRoom.createdBy}`);
  
  return { room: createdRoom, player };
}

const joinRoom = async (
  room: Room,
  player: Player,
  roomModel: ReturnModelType<typeof Room, {}>,
  playerModel: ReturnModelType<typeof Player, {}>
) => {
  const opponent = await playerModel.findOne({ user: player.user });
  const roomToJoin = await (await roomModel.findOne(room).populate('createdBy')).execPopulate();

  if (roomToJoin.full) throw new Error('Room was already full');
  
  roomToJoin.opponent = opponent;
  roomToJoin.full = true;
  await roomToJoin.save();

  console.info(`opponent: ${roomToJoin.opponent}`);
  console.info(`created room ${roomToJoin.name} & ${roomToJoin._id}`);
  return { room: roomToJoin, player };
}

const getRoomInfo = async (
  roomId: string,
  roomModel: ReturnModelType<typeof Room, {}>
) => {
  const room = await roomModel.findOne({ _id: roomId });
  
  const populated = await room
    .populate('createdBy')
    .populate('opponent')
    .execPopulate();
  
  return populated;
}

const closeRoom = async (
  roomId: string,
  roomModel: ReturnModelType<typeof Room, {}>
) => {

  const room = await roomModel.findOne({ _id: roomId });
  room.active = false;
  await room.save();

  return room;
}

const pushGameToRoom = async (
  roomId: string,
  game: Game,
  roomModel: ReturnModelType<typeof Room, {}>
) => {

  const room = await roomModel.findOne({ _id: roomId });
  room.games.push(game);
  await room.save();

  return room;
}

export { createRoom, joinRoom, getRoomInfo, closeRoom, pushGameToRoom }