import { Play } from '../model/play';
import { Game } from '../model/game';
import { Player } from '../model/player';
const startNewGame = (currentRoom: any) => {
  const playerRaffle: Player[] = [currentRoom.createdBy, currentRoom.opponent];
  playerRaffle.sort(() => Math.random() - 0.5);
          
  const newGame: Game = {
    started: true,
    hasWord: false,
    finished: false,
    word: '',
    wordThief: playerRaffle[0],
    detective: playerRaffle[1],
    questions: [],
    currentQuestion: null,
    winner: null
  };

  return newGame;
};
        
const chooseGameWord = (word: string, game: Game) => {
  game.hasWord = true;
  game.word = word;
  return game;
};

const makePlay = (game: Game, play: Play ) => {
  game.currentQuestion = play.question;

  if (play.wordGuess !== null) {
    if (play.wordGuess.toLowerCase() === game.word.toLowerCase()) {
      game.finished = true;
      game.winner = game.detective;
    }
  }

  if (play.question && play.question.response !== null) {
    game.questions.push(play.question);
    game.currentQuestion = null;
    if (game.questions.length >= 20) {
      game.finished = true;
      game.winner = game.wordThief;
    }
  }

  return game;
};

export { startNewGame , chooseGameWord, makePlay }