import { ReturnModelType } from '@typegoose/typegoose';
import { Player } from '../model/player';

const registerPlayer = async (
  user: string,
  password: string,
  name: string,
  playerModel: ReturnModelType<typeof Player, {}>
) => {
  const newPlayer = await playerModel.create({ user, password, name });
  await newPlayer.save();

  return newPlayer;
}

const resolvePlayer = async (
  user: string,
  playerModel: ReturnModelType<typeof Player, {}>,
) => {
  return await playerModel.findOne({ user });
};

export { registerPlayer, resolvePlayer }