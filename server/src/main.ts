import { GameServer } from './game.server';
import { Room } from './model/room';
import { Player } from './model/player'
import { getModelForClass } from '@typegoose/typegoose';
import * as mongoose from 'mongoose';
import 'reflect-metadata';

async function main(): Promise<any> {
  const existingMongoose = await mongoose.connect(
    'mongodb://mongo:27017/game',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }
  );

  const RoomModel = getModelForClass(Room, { existingMongoose })
  const PlayerModel = getModelForClass(Player, { existingMongoose })

  const gameServer = new GameServer(RoomModel, PlayerModel);
  const app = (await gameServer.connect()).app
  return { app };
} 

const bootsrap = main().then(() => console.log('Bootsrap finished...'));
export { bootsrap }